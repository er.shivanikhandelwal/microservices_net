﻿using Mango.Web.Models;

namespace Mango.Web.Services.IServices
{
    public interface IBaseService:IDisposable
    {
        ResultDTO ResponseModel { get; set; }
        Task<T> SendAsync<T>(APIRequest request);
    }
}
