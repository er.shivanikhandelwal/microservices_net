﻿using Mango.Web.Models;

namespace Mango.Web.Services.IServices
{
    public interface IProductService : IBaseService
    {
        Task<T> GetAllProducts<T>();
        Task<T> GetProductById<T>(int id);
        Task<T> CreateProduct<T>(ProductDTO product);
        Task<T> UpdateProduct<T>(ProductDTO product);
        Task<T> DeleteProduct<T>(int id);
    }
}
