﻿using Mango.Web.Models;
using Mango.Web.Services.IServices;

namespace Mango.Web.Services
{
    public class ProductService : BaseService, IProductService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public ProductService(IHttpClientFactory httpClientFactory):base(httpClientFactory)
        {
            _httpClientFactory=httpClientFactory;
        }
        public async Task<T> CreateProduct<T>(ProductDTO product)
        {
            return await this.SendAsync<T>(new APIRequest
            {
                APIType = SD.APIType.POST,
                AccessToken = "",
                Data = product,
                URL = SD.ProductAPIBase + "api/product/CreateProduct"
            });
        }

        public async Task<T> DeleteProduct<T>(int id)
        {
            return await this.SendAsync<T>(new APIRequest
            {
                APIType = SD.APIType.DELETE,
                AccessToken = "",
                URL = SD.ProductAPIBase + "api/product/DeleteProduct/" + id
            });
        }

        public async Task<T> GetAllProducts<T>()
        {
            return await this.SendAsync<T>(new APIRequest
            {
                APIType = SD.APIType.GET,
                AccessToken = "",
                URL = SD.ProductAPIBase + "api/product/Products"
            });
        }

        public async Task<T> GetProductById<T>(int id)
        {
            return await this.SendAsync<T>(new APIRequest
            {
                APIType = SD.APIType.GET,
                AccessToken = "",
                URL = SD.ProductAPIBase + "api/product/GetProductsById/" + id
            });
        }

        public async Task<T> UpdateProduct<T>(ProductDTO product)
        {
            return await this.SendAsync<T>(new APIRequest
            {
                APIType = SD.APIType.PUT,
                AccessToken = "",
                Data = product,
                URL = SD.ProductAPIBase + "api/product/UpdateProduct"
            });
        }
    }
}
