using AutoMapper;
using Mango.Service.ProductAPI;
using Mango.Service.ProductAPI.DbContexts;
using Mango.Service.ProductAPI.Repository;
using Microsoft.EntityFrameworkCore;
using Serilog;



Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Override("Microsoft",Serilog.Events.LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                //.ReadFrom.Configuration(builder.Configuration.GetSection("Serilog"))
                //.WriteTo.Seq("http://localhost:5341")
                .CreateBootstrapLogger();

try
{
    Log.Information("Serilog: Starting Application ...");
    var builder = WebApplication.CreateBuilder(args);

    builder.Host.UseSerilog((context, service, configuration) =>
    {
        configuration.ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(service)
        .Enrich.FromLogContext();
    });

    // Add services to the container.
    builder.Services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConncetion")));
    builder.Services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    //auto mapper configuration
    #region AutoMapper
    IMapper mapper = MappingConfig.RegisterMaps().CreateMapper();
    builder.Services.AddSingleton(mapper);
    builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
    #endregion

    //Add product service
    builder.Services.AddScoped<IProductRepository, ProductRepository>();

    var app = builder.Build();

    //hhtp request logging
    app.UseSerilogRequestLogging(configure =>
    {
        configure.MessageTemplate = "HTTP {RequestMethod} {RequestPath} ({UserId}) responded {StatusCode} in {Elapsed:0.0000}ms";
    });

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }
    app.UseHttpsRedirection();
    app.UseAuthorization();
    app.MapControllers();
    app.Run();
    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "Application terminated unexpectedly");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}
return 0;

