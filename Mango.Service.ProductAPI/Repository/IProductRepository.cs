﻿using Mango.Service.ProductAPI.Models.DTO;

namespace Mango.Service.ProductAPI.Repository
{
    public interface IProductRepository
    {
        //Task is used for create the method asynchronous
        Task<IEnumerable<ProductDTO>> GetProducts();
        Task<ProductDTO> GetProductById(int ProductId);
        Task<ProductDTO> CreateUpdateProduct(ProductDTO Product);
        Task<bool> DeleteProduct(int ProductId);
    }
}
