﻿using AutoMapper;
using Mango.Service.ProductAPI.DbContexts;
using Mango.Service.ProductAPI.Models;
using Mango.Service.ProductAPI.Models.DTO;
using Microsoft.EntityFrameworkCore;

namespace Mango.Service.ProductAPI.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _DbContext;
        private IMapper _mapper;

        public ProductRepository(ApplicationDbContext DbContext,IMapper mapper)
        {
            _DbContext = DbContext;
            _mapper = mapper;
        }
        public async Task<ProductDTO> CreateUpdateProduct(ProductDTO Product)
        {
            Product product= _mapper.Map<ProductDTO,Product>(Product);
            if(product.ProductId>0)
            {
                _DbContext.Products.Update(product);
            }
            else
            {
                _DbContext.Products.Add(product);
            }
            await _DbContext.SaveChangesAsync();
            return _mapper.Map<Product,ProductDTO>(product);
        }

        public async Task<bool> DeleteProduct(int ProductId)
        {
            try
            {
                Product productInfo = await _DbContext.Products.FirstOrDefaultAsync(a => a.ProductId == ProductId);
                if (productInfo == null)
                {
                    return false;
                }
                else
                {
                    _DbContext.Products.Remove(productInfo);
                    await _DbContext.SaveChangesAsync();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<ProductDTO> GetProductById(int ProductId)
        {
            Product product= await _DbContext.Products.FirstOrDefaultAsync(a=>a.ProductId==ProductId);
            return _mapper.Map<ProductDTO>(product);
        }

        public async Task<IEnumerable<ProductDTO>> GetProducts()
        {
            List<Product> productList = await _DbContext.Products.ToListAsync();
            return _mapper.Map<List<ProductDTO>>(productList);
        }
    }
}
