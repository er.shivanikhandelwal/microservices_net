﻿namespace Mango.Service.ProductAPI.Models.DTO
{
    public class ResultDTO
    {
        public bool IsSuccess{get;set;}=true;
        public object Result { get;set;}
        public string DisplayMessage { get; set; } = "";
        public List<string> ErrorMessage { get; set; }
    }
}
