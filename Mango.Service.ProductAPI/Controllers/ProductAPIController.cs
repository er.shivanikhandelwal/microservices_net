﻿using Mango.Service.ProductAPI.Models.DTO;
using Mango.Service.ProductAPI.Repository;
using Microsoft.AspNetCore.Mvc;

namespace Mango.Service.ProductAPI.Controllers
{
    [Route("api/product")]
    public class ProductAPIController : ControllerBase
    {
        protected ResultDTO _response;
        private IProductRepository _repository;
        private readonly ILogger<ProductAPIController> _logger;
        public ProductAPIController(IProductRepository repository,ILogger<ProductAPIController> logger)
        {
            _logger = logger;
            _repository = repository;
            _response = new ResultDTO();
        }
        /// <summary>
        /// Get All Products from Database
        /// </summary>
        /// <returns>All products</returns>
        [HttpGet("Products")]
        public async Task<ResultDTO> GetProducts()
        {
            try
            {
                IEnumerable<ProductDTO> products= await _repository.GetProducts();
                _response.Result=products;
                _response.IsSuccess=true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _response.IsSuccess=false;
                _response.ErrorMessage=new List<string> {ex.Message};
            }
            return _response;
        }
        /// <summary>
        /// Get Product By ID from Database
        /// </summary>
        /// <returns>Product Information</returns>
        [HttpGet("GetProductsById/{id}")]
        public async Task<ResultDTO> GetProductsById(int id)
        {
            try
            {
                ProductDTO product = await _repository.GetProductById(id);
                _response.Result = product;
                _response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _response.IsSuccess = false;
                _response.ErrorMessage = new List<string> { ex.Message };
            }
            return _response;
        }
        /// <summary>
        ///create new product in Database
        /// </summary>
        /// <returns>Product Information</returns>
        [HttpPost("CreateProduct")]
        public async Task<ResultDTO> CreateProduct([FromBody] ProductDTO product)
        {
            try
            {
                ProductDTO productResult = await _repository.CreateUpdateProduct(product);
                _response.Result = productResult;
                _response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _response.IsSuccess = false;
                _response.ErrorMessage = new List<string> { ex.Message };
            }
            return _response;
        }
        /// <summary>
        /// update product in Database
        /// </summary>
        /// <returns>Product Information</returns>
        [HttpPut("UpdateProduct")]
        public async Task<ResultDTO> UpdateProduct([FromBody] ProductDTO product)
        {
            try
            {
                ProductDTO productResult = await _repository.CreateUpdateProduct(product);
                _response.Result = productResult;
                _response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _response.IsSuccess = false;
                _response.ErrorMessage = new List<string> { ex.Message };
            }
            return _response;
        }
        /// <summary>
        ///create new product in Database
        /// </summary>
        /// <returns>Product Information</returns>
        [HttpDelete("DeleteProduct/{id}")]
        public async Task<ResultDTO> DeleteProduct(int id)
        {
            try
            {
                bool result = await _repository.DeleteProduct(id);
                _response.Result = result;
                _response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _response.IsSuccess = false;
                _response.ErrorMessage = new List<string> { ex.Message };
            }
            return _response;
        }
    }
}
